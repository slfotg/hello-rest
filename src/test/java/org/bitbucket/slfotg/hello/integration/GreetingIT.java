package org.bitbucket.slfotg.hello.integration;

import static org.junit.Assert.assertEquals;

import org.bitbucket.slfotg.hello.Greeting;
import org.bitbucket.slfotg.hello.GreetingController;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class GreetingIT {
    
    private final String NAME = "Sam";

    @Test
    public void testDefaultGreeting() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Greeting> result = restTemplate.getForEntity("http://172.17.0.1:18080/greeting", Greeting.class);
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(String.format(GreetingController.TEMPLATE, GreetingController.DEFAULT_NAME), result.getBody().getContent());
    }

    @Test
    public void testHelloSamGreeting() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Greeting> result = restTemplate.getForEntity("http://172.17.0.1:18080/greeting?name={0}",
                Greeting.class, NAME);
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(String.format(GreetingController.TEMPLATE, NAME), result.getBody().getContent());
    }
}
