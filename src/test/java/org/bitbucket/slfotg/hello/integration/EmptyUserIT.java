package org.bitbucket.slfotg.hello.integration;

import static org.junit.Assert.assertEquals;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class EmptyUserIT {

    @SuppressWarnings("rawtypes")
    @Test
    public void testEmptyUsers() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List> result = restTemplate.getForEntity("http://172.17.0.1:18080/user/all", List.class);
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(Collections.emptyList(), result.getBody());
    }
}
