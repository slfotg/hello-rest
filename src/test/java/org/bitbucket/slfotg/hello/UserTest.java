package org.bitbucket.slfotg.hello;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class UserTest {

    private User user;
    private final Integer ID = 1;
    private final String NAME = "Sam F";
    private final String EMAIL = "slfotg@gmail.com";

    @Before
    public void init() {
        user = new User();
        user.setId(ID);
        user.setName(NAME);
        user.setEmail(EMAIL);
    }

    @Test
    public void testGetId() {
        assertEquals(ID, user.getId());
    }

    @Test
    public void testGetName() {
        assertEquals(NAME, user.getName());
    }

    @Test
    public void testGetEmail() {
        assertEquals(EMAIL, user.getEmail());
    }
}
