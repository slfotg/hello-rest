package org.bitbucket.slfotg.hello;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    public static final String TEMPLATE = "Hello, %s!";
    public static final String DEFAULT_NAME = "World";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = DEFAULT_NAME) String name) {
        Greeting greeting = new Greeting();
        greeting.setId(counter.incrementAndGet());
        greeting.setContent(String.format(TEMPLATE, name));
        return greeting;
    }
}
