pipeline {
    agent any
    tools {
        maven "mvn-3"
        jdk "jdk-8"
    }
    // TODO post stages
    post {
        always {
            echo 'Done'
        }
        failure {
            echo 'Failure'
        }
        success {
            echo 'Success'
        }
    }
    stages {
        stage('Prepare') {
            steps {
                echo 'Starting'
                script {
                    COMMIT_HASH = sh(returnStdout: true, script: 'git rev-list HEAD --no-merges | head -n 1').trim()
                    notifyStash('INPROGRESS', COMMIT_HASH)
                }
            }
        }
        stage('Build') {
            steps {
                sh 'mvn clean package -DskipTests'
            }
        }
        stage('Test') {
            steps {
                sh 'mvn test'
            }
        }
        stage('Integration Test') {
            steps {
                sh 'mvn verify'
            }
        }
        stage('Sonar') {
            steps {
                script {
                    withSonarQubeEnv('sonar-demo') {
                        sh 'mvn sonar:sonar'
                    }
                }
            }
        }
        stage('Publish Artifacts') {
            steps {
                sh 'mvn deploy -DskipTests'
            }
        }
    }
}

def notifyStash(state, commit) {
    if('SUCCESS' == state || 'FAILURE' == state) {
        currentBuild.result = state
    }

    step([$class: 'StashNotifier',
          commitSha1: "${commit}",
          credentialsId: "slfotg-bitbucket",
          disableInprogressNotification: false,
          ignoreUnverifiedSSLPeer: true,
          includeBuildNumberInKey: false,
          prependParentProjectKey: false,
          projectKey: '',
          stashServerBaseUrl: '']) //Stash URL should be configured in Jenkins Manage
}
