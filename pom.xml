<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.0.3.RELEASE</version>
    </parent>
    <groupId>org.bitbucket.slfotg</groupId>
    <artifactId>hello-rest</artifactId>
    <version>0.0.1</version>

    <distributionManagement>
        <repository>
            <id>demo</id>
            <url>https://nexus.demo.com/repository/demo/</url>
        </repository>
    </distributionManagement>

    <properties>
        <docker.maven.plugin.fabric8.version>0.26.1</docker.maven.plugin.fabric8.version>
        <jacoco.version>0.8.1</jacoco.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-jpa</artifactId>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.jayway.jsonpath</groupId>
            <artifactId>json-path</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <finalName>${project.artifactId}</finalName>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>

            <plugin>
                <groupId>io.fabric8</groupId>
                <artifactId>docker-maven-plugin</artifactId>
                <version>${docker.maven.plugin.fabric8.version}</version>
                <configuration>
                    <logDate>default</logDate>
                    <autoPull>true</autoPull>
                    <images>
                        <image>
                            <alias>db</alias>
                            <name>mysql:5.7.22</name>
                            <run>
                                <env>
                                    <MYSQL_ROOT_PASSWORD>root_password</MYSQL_ROOT_PASSWORD>
                                    <MYSQL_DATABASE>db_example</MYSQL_DATABASE>
                                    <MYSQL_USER>springuser</MYSQL_USER>
                                    <MYSQL_PASSWORD>ThePassword</MYSQL_PASSWORD>
                                </env>
                                <ports>
                                    <port>13306:3306</port>
                                </ports>
                                <wait>
                                    <log>socket: '/var/run/mysqld/mysqld.sock'.*port: 3306</log>
                                    <time>60000</time>
                                </wait>
                                <log>
                                    <prefix>MySQL </prefix>
                                    <color>yellow</color>
                                </log>
                            </run>
                        </image>

                        <image>
                            <alias>hello-rest</alias>
                            <name>${project.artifactId}:${project.version}</name>

                            <build>
                                <from>openjdk:8</from>
                                <assembly>
                                    <descriptor>${basedir}/src/main/docker/assembly.xml</descriptor>
                                </assembly>

                                <ports>
                                    <port>8080</port>
                                </ports>
                                <cmd>java -jar /maven/hello-rest.jar</cmd>
                            </build>

                            <run>
                                <ports>
                                    <port>18080:8080</port>
                                </ports>
                                <wait>
                                    <url>http://${docker.host.address}:18080/greeting</url>
                                    <time>60000</time>
                                </wait>
                                <links>
                                    <link>db:db</link>
                                </links>
                                <log>
                                    <prefix>REST </prefix>
                                    <color>cyan</color>
                                </log>
                            </run>
                        </image>
                    </images>
                </configuration>
                <!-- Hooking into the lifecycle -->
                <executions>
                    <execution>
                        <id>start</id>
                        <phase>pre-integration-test</phase>
                        <goals>
                            <goal>build</goal>
                            <goal>start</goal>
                        </goals>
                        <configuration>
                            <skip>${skipITs}</skip>
                        </configuration>
                    </execution>
                    <execution>
                        <id>stop</id>
                        <phase>post-integration-test</phase>
                        <goals>
                            <goal>stop</goal>
                        </goals>
                        <configuration>
                            <skip>${skipITs}</skip>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <artifactId>maven-failsafe-plugin</artifactId>
            </plugin>

            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <showWarnings>true</showWarnings>
                    <compilerArgs>
                        <arg>-Xlint:all</arg>
                    </compilerArgs>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>${jacoco.version}</version>
                <executions>
                    <execution>
                        <id>default-prepare-agent</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>default-report</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

        </plugins>
    </build>

    <profiles>
        <profile>
            <id>skip-integration-tests</id>
            <activation>
                <property>
                    <name>skipTests</name>
                </property>
            </activation>
            <properties>
                <skipITs>true</skipITs>
            </properties>
        </profile>
    </profiles>

    <repositories>
        <repository>
            <id>spring-releases</id>
            <url>https://repo.spring.io/libs-release</url>
        </repository>
    </repositories>
    <pluginRepositories>
        <pluginRepository>
            <id>spring-releases</id>
            <url>https://repo.spring.io/libs-release</url>
        </pluginRepository>
    </pluginRepositories>
</project>
